#ifndef INTERFACE_H
#define INTERFACE_H

#include <QObject>

class cipher {
public:
  //! @brief Создает шифр.
  //! @param[in] Name - имя шифра
  cipher(const QString& Name) { name = Name; }

  //! @brief Применяет алгоритм шифрования.
  //! @param[in] Text - текст, поступающий на шифрование
  //! @param[in] lang - язык, выбранный в *MenuBar*
  //! @returns Зашифрованный текст
  virtual QString Apply(const QString& Text, const QString& lang) = 0;

  //! @brief Применяет алгоритм дешифрования.
  //! @param[in] Text - текст, поступающий на шифрование
  //! @param[in] lang - язык, выбранный в *MenuBar*
  //! @returns Расшифрованный текст
  virtual QString Revent(const QString& Text, const QString& lang) = 0;

  //! @brief Создает окно с настройками шифра.
  virtual void createOptions() { };

  //! @brief Возвращает указатель на окно с настройками шифра. Если окна нет - возвращает *nullptr*.
  virtual QWidget* returnOptions() { return nullptr; }

  //! @brief Возвращает имя шифра.
  QString getName() { return name; }
protected:
  QString name; //!< имя шифра
};

#define cipher_iid "ru.some.how.it.must.work"
Q_DECLARE_INTERFACE(cipher, cipher_iid)

#endif // INTERFACE_H
