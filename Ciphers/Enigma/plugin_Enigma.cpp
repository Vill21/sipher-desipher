#include "plugin_Enigma.h"

Enigma::Enigma(QObject *parent) : QObject (parent), cipher("Enigma") {
  setRotors();
  createOptions();
}

Enigma::~Enigma() {
  delete[] rotorI;
  delete[] rotorII;
  delete[] rotorIII;

  delete[] rotorI_reverse;
  delete[] rotorII_reverse;
  delete[] rotorIII_reverse;

  delete[] rotorI_ru;
  delete[] rotorII_ru;
  delete[] rotorIII_ru;

  delete[] rotorI_ru_reverse;
  delete[] rotorII_ru_reverse;
  delete[] rotorIII_ru_reverse;

  delete[] reflector ;
  delete[] reflector_ru ;

  delete[] en;
  delete[] Chosen_en;
  delete[] panel;
  delete[] Chosen_en_reverse;
  delete[] panel_reverse;

  delete[] ru;
  delete[] Chosen_ru;
  delete[] panel_ru;
  delete[] Chosen_ru_reverse;
  delete[] panel_ru_reverse;
}

void Enigma::setRotors(){

  en = new QChar[26];

  QString alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  std::ptrdiff_t ind(0);
  int ind_str(0);
  do {
    en[ind] = alphabet[ind_str];
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  ru = new QChar[33];
  alphabet = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
  ind = 0;
  ind_str = 0;
  do {
    ru[ind] = alphabet[ind_str];
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  rotorI = new std::ptrdiff_t[26];
  rotorI_reverse = new std::ptrdiff_t[26];

  alphabet = "EKMFLGDQVZNTOWYHXUSPAIBRCJ";
  ind = 0;
  ind_str = 0;
  do {
    std::ptrdiff_t index(find_en(alphabet[ind_str]));
    rotorI[ind] = index - ind;
    rotorI_reverse[index] = -(index - ind);
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  rotorI_ru = new std::ptrdiff_t[33];
  rotorI_ru_reverse = new std::ptrdiff_t[33];

  alphabet = "АГВДБКЁЗИЖЙЛЕОРПНУХЮМЦЭЧЯСЩЬТЪФШЫ";
  ind = 0;
  ind_str = 0;
  do {
    std::ptrdiff_t index(find_ru(alphabet[ind_str]));
    rotorI_ru[ind] = index - ind;
    rotorI_ru_reverse[index] = -(index - ind);
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  rotorII = new std::ptrdiff_t[26];
  rotorII_reverse = new std::ptrdiff_t[26];

  alphabet = "AJDKSIRUXBLHWTMCQGZNPYFVOE";
  ind = 0;
  ind_str = 0;
  do {
    std::ptrdiff_t index(find_en(alphabet[ind_str]));
    rotorII[ind] = index - ind;
    rotorII_reverse[index] = -(index - ind);
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  rotorII_ru = new std::ptrdiff_t[33];
  rotorII_ru_reverse = new std::ptrdiff_t[33];

  alphabet = "АГСОЛПРЬУВЙИТЦЮЖЭЯЧХЪЩФШЕКДБМЫЁНЗ";
  ind = 0;
  ind_str = 0;
  do {
    std::ptrdiff_t index(find_ru(alphabet[ind_str]));
    rotorII_ru[ind] = index - ind;
    rotorII_ru_reverse[index] = -(index - ind);
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  rotorIII = new std::ptrdiff_t[26];
  rotorIII_reverse = new std::ptrdiff_t[26];

  alphabet = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
  ind = 0;
  ind_str = 0;
  do {
    std::ptrdiff_t index(find_en(alphabet[ind_str]));
    rotorIII[ind] = index - ind;
    rotorIII_reverse[index] = -(index - ind);
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  rotorIII_ru = new std::ptrdiff_t[33];
  rotorIII_ru_reverse = new std::ptrdiff_t[33];

  alphabet = "ПРОНЕТВЖЭХЁЮЗСЯЧГАЙШУЪЬИЦЫДЛМФКЩБ";
  ind = 0;
  ind_str = 0;
  do {
    std::ptrdiff_t index(find_ru(alphabet[ind_str]));
    rotorIII_ru[ind] = index - ind;
    rotorIII_ru_reverse[index] = -(index - ind);
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  panel = new std::ptrdiff_t[26];
  panel_reverse = new std::ptrdiff_t[26];

  alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  ind = 0;
  ind_str = 0;
  do {
    std::ptrdiff_t index(find_en(alphabet[ind_str]));
    panel[ind] = index - ind;
    panel_reverse[index] = -(index - ind);
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  panel_ru = new std::ptrdiff_t[33];
  panel_ru_reverse = new std::ptrdiff_t[33];

  alphabet = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
  ind = 0;
  ind_str = 0;
  do {
    std::ptrdiff_t index(find_ru(alphabet[ind_str]));
    panel_ru[ind] = index - ind;
    panel_ru_reverse[index] = -(index - ind);
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  reflector = new std::ptrdiff_t[26];

  alphabet = "QYHOGNECVPUZTFDJAXWMKISRBL";
  ind = 0;
  ind_str = 0;
  do {
    reflector[ind] = find_en(alphabet[ind_str]) ;
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  reflector_ru = new std::ptrdiff_t[33];

  alphabet = "ВЕАИЯБКЧЬГОЁТХРЙСНПЛФУМШЖЦЮЭЫЗЪЩД";
  ind = 0;
  ind_str = 0;
  do {
    reflector_ru[ind] = find_ru(alphabet[ind_str]) ;
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  Chosen_en = new std::ptrdiff_t*[3];
  Chosen_en[0] = rotorI;
  Chosen_en[1] = rotorII;
  Chosen_en[2] = rotorIII;

  Chosen_en_reverse = new std::ptrdiff_t*[3];
  Chosen_en_reverse[2] = rotorIII_reverse;
  Chosen_en_reverse[1] = rotorII_reverse;
  Chosen_en_reverse[0] = rotorI_reverse;

  Chosen_ru = new std::ptrdiff_t*[3];
  Chosen_ru[0] = rotorI_ru;
  Chosen_ru[1] = rotorII_ru;
  Chosen_ru[2] = rotorIII_ru;

  Chosen_ru_reverse = new std::ptrdiff_t*[3];
  Chosen_ru_reverse[0] = rotorI_ru_reverse;
  Chosen_ru_reverse[1] = rotorII_ru_reverse;
  Chosen_ru_reverse[2] = rotorIII_ru_reverse;
}

QString Enigma::Apply(const QString& Text, const QString& lang) {
  QString txt_out(Text.toUpper());
  QString out("");

  if (lang == "En") {
    std::ptrdiff_t ind_(0);
    std::ptrdiff_t* ind = new std::ptrdiff_t[in_use_en];
    do {
      ind[ind_] = 0;
      ind_ += 1;
    } while (ind_ < in_use_en);

    int ind_str(0);

    do {
      std::ptrdiff_t res = find_en(txt_out[ind_str]);
      if (res != -1) {
        //Шифровка

        std::ptrdiff_t next(0);

        //Смешение по коммутатору

        next = panel[res];
        res = (res + next) % 26;
        if (res < 0) {
          res += 26;
        }

        //Смещение по роторам

        for (std::ptrdiff_t i = 0; i < in_use_en; i += 1) {
          std::ptrdiff_t fin_res((res - ind[i]) % 26);
          if (fin_res < 0) {
            fin_res += 26;
          }
          next = Chosen_en[i][fin_res];
          res = (res + next) % 26;
          if (res < 0) {
            res += 26;
          }
        }

        //Рефлектор

        res = reflector[res];

        //Смещение по роторам обратно

        for (std::ptrdiff_t i = in_use_en - 1; i >= 0; i -= 1) {
          std::ptrdiff_t fin_res((res - ind[i]) % 26);
          if (fin_res < 0) {
            fin_res += 26;
          }
          next = Chosen_en_reverse[i][fin_res];
          res = (res + next) % 26;
          if (res < 0) {
            res += 26;
          }
        }

        //Смещение по коммутатору обратно

        next = panel_reverse[res];
        res = (res + next) % 26;
        if (res < 0) {
          res += 26;
        }

        if (Text[ind_str].isLower())
          txt_out[ind_str] = en[res].toLower();
        else
          txt_out[ind_str] = en[res];

        //Вращение роторов

        ind[0] = ind[0] + 1;
        for (std::ptrdiff_t i = 0; i < in_use_en - 1; ++i) {
          if (ind[i] == 26) {
            ind[i + 1] = ind[i + 1] + 1;
            ind[i] = 0;
          }
        }
        if (ind[in_use_en - 1] == 26)
          ind[in_use_en - 1] = 0;
      }
      else {
        txt_out[ind_str] = Text[ind_str];
      }
      ind_str += 1;
    } while (ind_str < txt_out.length());
    out += "\n" + QString::number(in_use_en) + "\n";
    chosen_en_nums(out);
  }
  if (lang == "Ru"){
    std::ptrdiff_t ind_(0);
    std::ptrdiff_t* ind = new std::ptrdiff_t[in_use_ru];
    do {
      ind[ind_] = 0;
      ind_ += 1;
    } while (ind_ < in_use_ru);

    int ind_str(0);

    do {
      std::ptrdiff_t res = find_ru(txt_out[ind_str]);
      if (res != -1) {
        //Шифровка

        std::ptrdiff_t next(0);

        //Смешение по коммутатору

        next = panel_ru[res];
        res = (res + next) % 33;
        if (res < 0) {
          res += 33;
        }

        //Смещение по роторам

        for (std::ptrdiff_t i = 0; i < in_use_ru; i += 1) {
          std::ptrdiff_t fin_res((res - ind[i]) % 33);
          if (fin_res < 0) {
            fin_res += 33;
          }
          next = Chosen_ru[i][fin_res];
          res = (res + next) % 33;
          if (res < 0) {
            res += 33;
          }
        }

        //Рефлектор

        res = reflector_ru[res];

        //Смещение по роторам обратно

        for (std::ptrdiff_t i = in_use_ru - 1; i >= 0; i -= 1) {
          std::ptrdiff_t fin_res((res - ind[i]) % 33);
          if (fin_res < 0) {
            fin_res += 33;
          }
          next = Chosen_ru_reverse[i][fin_res];
          res = (res + next) % 33;
          if (res < 0) {
            res += 33;
          }
        }

        //Смещение по коммутатору обратно

        next = panel_ru_reverse[res];
        res = (res + next) % 33;
        if (res < 0) {
          res += 33;
        }

        if (Text[ind_str].isLower())
          txt_out[ind_str] = ru[res].toLower();
        else
          txt_out[ind_str] = ru[res];

        //Вращение роторов

        ind[0] = ind[0] + 1;
        for (std::ptrdiff_t i = 0; i < in_use_ru - 1; ++i) {
          if (ind[i] == 33) {
            ind[i + 1] = ind[i + 1] + 1;
            ind[i] = 0;
          }
        }
        if (ind[in_use_ru - 1] == 33)
          ind[in_use_ru - 1] = 0;
      }
      else {
        txt_out[ind_str] = Text[ind_str];
      }
      ind_str += 1;
    } while (ind_str < txt_out.length());
    out += "\n" + QString::number(in_use_ru) + "\n";
    chosen_ru_nums(out);
  }
  return txt_out + " " + out;
}

QString Enigma::Revent(const QString& Text, const QString& lang){
  QString txt_out(Text.toUpper());

  if (lang == "En") {
    std::ptrdiff_t ind_(0);
    std::ptrdiff_t* ind = new std::ptrdiff_t[in_use_en];
    do {
      ind[ind_] = 0;
      ind_ += 1;
    } while (ind_ < in_use_en);

    int ind_str(0);

    do {
      std::ptrdiff_t res = find_en(txt_out[ind_str]);
      if (res != -1) {
        //Шифровка

        std::ptrdiff_t next(0);

        //Смешение по коммутатору

        next = panel[res];
        res = (res + next) % 26;
        if (res < 0) {
          res += 26;
        }

        //Смещение по роторам

        for (std::ptrdiff_t i = 0; i < in_use_en; i += 1) {
          std::ptrdiff_t fin_res((res - ind[i]) % 26);
          if (fin_res < 0) {
            fin_res += 26;
          }
          next = Chosen_en[i][fin_res];
          res = (res + next) % 26;
          if (res < 0) {
            res += 26;
          }
        }

        //Рефлектор

        res = reflector[res];

        //Смещение по роторам обратно

        for (std::ptrdiff_t i = in_use_en - 1; i >= 0; i -= 1) {
          std::ptrdiff_t fin_res((res - ind[i]) % 26);
          if (fin_res < 0) {
            fin_res += 26;
          }
          next = Chosen_en_reverse[i][fin_res];
          res = (res + next) % 26;
          if (res < 0) {
            res += 26;
          }
        }

        //Смещение по коммутатору обратно

        next = panel_reverse[res];
        res = (res + next) % 26;
        if (res < 0) {
          res += 26;
        }

        if (Text[ind_str].isLower())
          txt_out[ind_str] = en[res].toLower();
        else
          txt_out[ind_str] = en[res];

        //Вращение роторов

        ind[0] = ind[0] + 1;
        for (std::ptrdiff_t i = 0; i < in_use_en - 1; ++i) {
          if (ind[i] == 26) {
            ind[i + 1] = ind[i + 1] + 1;
            ind[i] = 0;
          }
        }
        if (ind[in_use_en - 1] == 26)
          ind[in_use_en - 1] = 0;
      }
      else {
        txt_out[ind_str] = Text[ind_str];
      }
      ind_str += 1;
    } while (ind_str < txt_out.length());
  }
  if (lang == "Ru"){
    std::ptrdiff_t ind_(0);
    std::ptrdiff_t* ind = new std::ptrdiff_t[in_use_ru];
    do {
      ind[ind_] = 0;
      ind_ += 1;
    } while (ind_ < in_use_ru);

    int ind_str(0);

    do {
      std::ptrdiff_t res = find_ru(txt_out[ind_str]);
      if (res != -1) {
        //Шифровка

        std::ptrdiff_t next(0);

        //Смешение по коммутатору

        next = panel_ru[res];
        res = (res + next) % 33;
        if (res < 0) {
          res += 33;
        }

        //Смещение по роторам

        for (std::ptrdiff_t i = 0; i < in_use_ru; i += 1) {
          std::ptrdiff_t fin_res((res - ind[i]) % 33);
          if (fin_res < 0) {
            fin_res += 33;
          }
          next = Chosen_ru[i][fin_res];
          res = (res + next) % 33;
          if (res < 0) {
            res += 33;
          }
        }

        //Рефлектор

        res = reflector_ru[res];

        //Смещение по роторам обратно

        for (std::ptrdiff_t i = in_use_ru - 1; i >= 0; i -= 1) {
          std::ptrdiff_t fin_res((res - ind[i]) % 33);
          if (fin_res < 0) {
            fin_res += 33;
          }
          next = Chosen_ru_reverse[i][fin_res];
          res = (res + next) % 33;
          if (res < 0) {
            res += 33;
          }
        }

        //Смещение по коммутатору обратно

        next = panel_ru_reverse[res];
        res = (res + next) % 33;
        if (res < 0) {
          res += 33;
        }

        if (Text[ind_str].isLower())
          txt_out[ind_str] = ru[res].toLower();
        else
          txt_out[ind_str] = ru[res];

        //Вращение роторов

        ind[0] = ind[0] + 1;
        for (std::ptrdiff_t i = 0; i < in_use_ru - 1; ++i) {
          if (ind[i] == 33) {
            ind[i + 1] = ind[i + 1] + 1;
            ind[i] = 0;
          }
        }
        if (ind[in_use_ru - 1] == 33)
          ind[in_use_ru - 1] = 0;
      }
      else {
        txt_out[ind_str] = Text[ind_str];
      }
      ind_str += 1;
    } while (ind_str < txt_out.length());
  }

  return txt_out;
}

std::ptrdiff_t Enigma::find_en(QChar ch){
  std::ptrdiff_t ind(0);
  std::ptrdiff_t find(-1);
  do {
    if (en[ind] == ch){
      find = ind;
      break;
    }
    ++ind;
  } while (ind < 26);
  return find;
}

std::ptrdiff_t Enigma::find_ru(QChar ch){
  std::ptrdiff_t ind(0);
  std::ptrdiff_t find(-1);
  do {
    if (ru[ind] == ch){
      find = ind;
      break;
    }
    ++ind;
  } while (ind < 33);
  return find;
}

void Enigma::createOptions() {
  widget = new QWidget;

  line_rotorI = new QLineEdit("EKMFLGDQVZNTOWYHXUSPAIBRCJ");
  line_rotorII = new QLineEdit("AJDKSIRUXBLHWTMCQGZNPYFVOE");
  line_rotorIII = new QLineEdit("BDFHJLCPRTXVZNYEIWGAKMUSQO");

  line_rotorI->setReadOnly(true);
  line_rotorII->setReadOnly(true);
  line_rotorIII->setReadOnly(true);

  line_rotorI_ru = new QLineEdit("АГВДБКЁЗИЖЙЛЕОРПНУХЮМЦЭЧЯСЩЬТЪФШЫ");
  line_rotorII_ru = new QLineEdit("АГСОЛПРЬУВЙИТЦЮЖЭЯЧХЪЩФШЕКДБМЫЁНЗ");
  line_rotorIII_ru = new QLineEdit("ПРОНЕТВЖЭХЁЮЗСЯЧГАЙШУЪЬИЦЫДЛМФКЩБ");

  line_rotorI_ru->setReadOnly(true);
  line_rotorII_ru->setReadOnly(true);
  line_rotorIII_ru->setReadOnly(true);

  line_panel_ru = new QLineEdit("АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ");
  line_panel = new QLineEdit("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

  ru_spin = new QSpinBox();
  ru_spin->setValue(3);
  ru_spin->setMaximum(3);
  ru_spin->setMinimum(1);

  ru_spin_1 = new QSpinBox();
  ru_spin_1->setValue(1);
  ru_spin_1->setMaximum(3);
  ru_spin_1->setMinimum(1);

  ru_spin_2 = new QSpinBox();
  ru_spin_2->setValue(2);
  ru_spin_2->setMaximum(3);
  ru_spin_2->setMinimum(1);

  ru_spin_3 = new QSpinBox();
  ru_spin_3->setValue(3);
  ru_spin_3->setMaximum(3);
  ru_spin_3->setMinimum(1);

  en_spin = new QSpinBox();
  en_spin->setValue(3);
  en_spin->setMaximum(3);
  en_spin->setMinimum(1);

  en_spin_1 = new QSpinBox();
  en_spin_1->setValue(1);
  en_spin_1->setMaximum(3);
  en_spin_1->setMinimum(1);

  en_spin_2 = new QSpinBox();
  en_spin_2->setValue(2);
  en_spin_2->setMaximum(3);
  en_spin_2->setMinimum(1);

  en_spin_3 = new QSpinBox();
  en_spin_3->setValue(3);
  en_spin_3->setMaximum(3);
  en_spin_3->setMinimum(1);

  QPushButton* button_en = new QPushButton("Accept En panel");
  QPushButton* button_ru = new QPushButton("Accept Ru panel");

  QLabel* label_en_num_used = new QLabel();
  label_en_num_used->setText("En rotors max num used: ");
  QLabel* label_ru_num_used = new QLabel();
  label_ru_num_used->setText("Ru rotors max num used: ");
  QLabel* label_en_used = new QLabel();
  label_en_used->setText("En rotors used: ");
  QLabel* label_ru_used = new QLabel();
  label_ru_used->setText("Ru rotors used: ");
  QLabel* label_en_rotors = new QLabel();
  label_en_rotors->setText("En rotors: ");
  QLabel* label_ru_rotors = new QLabel();
  label_ru_rotors->setText("Ru rotors: ");
  QLabel* label_panel = new QLabel();
  label_panel->setText("En panel: ");
  QLabel* label_panel_ru = new QLabel();
  label_panel_ru->setText("Ru panel: ");

  connect(ru_spin, SIGNAL(valueChanged(int)), this, SLOT(on_spin_ru_valueChanged(int)));
  connect(en_spin, SIGNAL(valueChanged(int)), this, SLOT(on_spin_en_valueChanged(int)));
  connect(ru_spin_1, SIGNAL(valueChanged(int)), this, SLOT(on_spin_ru_1_valueChanged(int)));
  connect(en_spin_1, SIGNAL(valueChanged(int)), this, SLOT(on_spin_en_1_valueChanged(int)));
  connect(ru_spin_2, SIGNAL(valueChanged(int)), this, SLOT(on_spin_ru_2_valueChanged(int)));
  connect(en_spin_2, SIGNAL(valueChanged(int)), this, SLOT(on_spin_en_2_valueChanged(int)));
  connect(ru_spin_3, SIGNAL(valueChanged(int)), this, SLOT(on_spin_ru_3_valueChanged(int)));
  connect(en_spin_3, SIGNAL(valueChanged(int)), this, SLOT(on_spin_en_3_valueChanged(int)));
  connect(button_en, SIGNAL(clicked()), this, SLOT(on_button_en_clicked()));
  connect(button_ru, SIGNAL(clicked()), this, SLOT(on_button_ru_clicked()));

  QHBoxLayout* label1 = new QHBoxLayout;
  label1->addWidget(label_en_num_used);
  label1->addWidget(label_ru_num_used);
  QHBoxLayout* label2 = new QHBoxLayout;
  label2->addWidget(label_en_used);
  QHBoxLayout* label3 = new QHBoxLayout;
  label3->addWidget(label_ru_used);
  QHBoxLayout* label4 = new QHBoxLayout;
  label4->addWidget(label_en_rotors);
  label4->addWidget(label_ru_rotors);
  QHBoxLayout* label5 = new QHBoxLayout;
  label5->addWidget(label_panel);
  label5->addWidget(label_panel_ru);
  QHBoxLayout* rus_spins = new QHBoxLayout;
  rus_spins->addWidget(ru_spin_1);
  rus_spins->addWidget(ru_spin_2);
  rus_spins->addWidget(ru_spin_3);
  QHBoxLayout* eng_spins = new QHBoxLayout;
  eng_spins->addWidget(en_spin_1);
  eng_spins->addWidget(en_spin_2);
  eng_spins->addWidget(en_spin_3);
  QHBoxLayout* used = new QHBoxLayout;
  used->addWidget(en_spin);
  used->addWidget(ru_spin);
  QHBoxLayout* rotor_1 = new QHBoxLayout;
  rotor_1->addWidget(line_rotorI);
  rotor_1->addWidget(line_rotorI_ru);
  QHBoxLayout* rotor_2 = new QHBoxLayout;
  rotor_2->addWidget(line_rotorII);
  rotor_2->addWidget(line_rotorII_ru);
  QHBoxLayout* rotor_3 = new QHBoxLayout;
  rotor_3->addWidget(line_rotorIII);
  rotor_3->addWidget(line_rotorIII_ru);
  QHBoxLayout* panels = new QHBoxLayout;
  panels->addWidget(line_panel);
  panels->addWidget(line_panel_ru);
  QHBoxLayout* buttons = new QHBoxLayout;
  buttons->addWidget(button_en);
  buttons->addWidget(button_ru);
  QFormLayout* layout = new QFormLayout;
  layout->addRow(label1);
  layout->addRow(used);
  layout->addRow(label2);
  layout->addRow(eng_spins);
  layout->addRow(label3);
  layout->addRow(rus_spins);
  layout->addRow(label4);
  layout->addRow(rotor_1);
  layout->addRow(rotor_2);
  layout->addRow(rotor_3);
  layout->addRow(label5);
  layout->addRow(panels);
  layout->addRow(buttons);

  widget->setLayout(layout);
}

QWidget* Enigma::returnOptions() {
  return widget;
}

void Enigma::on_spin_ru_valueChanged(int arg1) {
  in_use_ru = arg1;
}
void Enigma::on_spin_en_valueChanged(int arg1) {
  in_use_en = arg1;
}

void Enigma::on_spin_ru_1_valueChanged(int arg1){
  choose_ru(0, arg1);
}
void Enigma::on_spin_en_1_valueChanged(int arg1){
  choose_en(0, arg1);
}
void Enigma::on_spin_ru_2_valueChanged(int arg1){
  choose_ru(1, arg1);
}
void Enigma::on_spin_en_2_valueChanged(int arg1){
  choose_en(1, arg1);
}
void Enigma::on_spin_ru_3_valueChanged(int arg1){
  choose_ru(2, arg1);
}
void Enigma::on_spin_en_3_valueChanged(int arg1){
  choose_en(2, arg1);
}

void Enigma::choose_en(const std::ptrdiff_t& num, const int& val){
  if (val == 1) {
    Chosen_en[num] = rotorI;
    Chosen_en_reverse[num] = rotorI_reverse;
  }
  if (val == 2) {
    Chosen_en[num] = rotorII;
    Chosen_en_reverse[num] = rotorII_reverse;
  }
  if (val == 3) {
    Chosen_en[num] = rotorIII;
    Chosen_en_reverse[num] = rotorIII_reverse;
  }
}
void Enigma::choose_ru(const std::ptrdiff_t& num, const int& val){
  if (val == 1) {
    Chosen_ru[num] = rotorI_ru;
    Chosen_ru_reverse[num] = rotorI_ru_reverse;
  }
  if (val == 2) {
    Chosen_ru[num] = rotorII_ru;
    Chosen_ru_reverse[num] = rotorII_ru_reverse;
  }
  if (val == 3) {
    Chosen_ru[num] = rotorIII_ru;
    Chosen_ru_reverse[num] = rotorIII_ru_reverse;
  }
}
void Enigma::chosen_en_nums(QString& out){
  for (std::ptrdiff_t i = 0; i < in_use_en; i += 1) {
    if (Chosen_en[i] == rotorI){
      out += QString::number(1) + " ";
    }
    if (Chosen_en[i] == rotorII){
      out += QString::number(2) + " ";
    }
    if (Chosen_en[i] == rotorIII){
      out += QString::number(3) + " ";
    }
  }
}
void Enigma::chosen_ru_nums(QString& out){
  for (std::ptrdiff_t i = 0; i < in_use_en; i += 1) {
    if (Chosen_ru[i] == rotorI_ru){
      out += QString::number(1) + " ";
    }
    if (Chosen_ru[i] == rotorII_ru){
      out += QString::number(2) + " ";
    }
    if (Chosen_ru[i] == rotorIII_ru){
      out += QString::number(3) + " ";
    }
  }
}
void Enigma::on_button_en_clicked() {
  std::ptrdiff_t res(-1);
  QString txt = line_panel->text().toUpper();
  for (int i = 0; i < txt.length(); ++i) {
    res = find_en(txt[i]);
    if (res == -1) {
      break;
    }
  }
  if (res == -1) {
    line_panel->setText("Error: non english words used - " + txt);
  }
  if (txt.length() > 26) {
    line_panel->setText("Error: too many english words used - " + txt);
  }
  else {
    QString new_panel("");
    for (int i = 0; i < txt.length(); i += 1) {
      new_panel += txt[i];
    }
    for (int i = txt.length(); i < 26; i += 1) {
      new_panel += en[i];
    }
    line_panel->setText(new_panel);

    bool check(false);
    for (int i = 0; i < new_panel.length() - 1; i += 1) {
      if (check)
        break;
      for (int j = i + 1; j < new_panel.length(); j += 1) {
        if (new_panel[i] == new_panel[j]) {
          check = true;
          break;
        }
      }
    }
    if (check) {
      line_panel->setText("Error: found duplicate in text - " + txt);
    }
    else {
      std::ptrdiff_t ind = 0;
      int ind_str = 0;
      do {
        std::ptrdiff_t index(find_en(txt[ind_str]));
        panel[ind] = index - ind;
        panel_reverse[index] = -(index - ind);
        ++ind;
        ++ind_str;
      } while (ind_str < txt.length());
      std::ptrdiff_t ind_alph = ind_str;

      while (ind_alph < 26) {
        panel[ind] = ind_alph - ind;
        panel_reverse[ind_alph] = -(ind_alph - ind);
        ++ind;
        ++ind_alph;
      }
    }
  }
}
void Enigma::on_button_ru_clicked() {
  std::ptrdiff_t res(-1);
  QString txt = line_panel_ru->text().toUpper();
  for (int i = 0; i < txt.length(); ++i) {
    res = find_ru(txt[i]);
    if (res == -1) {
      break;
    }
  }
  if (res == -1) {
    line_panel_ru->setText("Error: non russian words used - " + txt);
  }
  if (txt.length() > 33) {
    line_panel_ru->setText("Error: too many russian words used - " + txt);
  }
  else {
    QString new_panel("");
    for (int i = 0; i < txt.length(); i += 1) {
      new_panel += txt[i];
    }
    for (int i = txt.length(); i < 33; i += 1) {
      new_panel += ru[i];
    }
    line_panel_ru->setText(new_panel);

    bool check(false);
    for (int i = 0; i < new_panel.length() - 1; i += 1) {
      if (check)
        break;
      for (int j = i + 1; j < new_panel.length(); j += 1) {
        if (new_panel[i] == new_panel[j]) {
          check = true;
          break;
        }
      }
    }
    if (check) {
      line_panel_ru->setText("Error: found duplicate in text - " + txt);
    }
    else {
      std::ptrdiff_t ind = 0;
      int ind_str = 0;
      do {
        std::ptrdiff_t index(find_ru(txt[ind_str]));
        panel_ru[ind] = index - ind;
        panel_ru_reverse[index] = -(index - ind);
        ++ind;
        ++ind_str;
      } while (ind_str < txt.length());
      std::ptrdiff_t ind_alph = ind_str;

      while (ind_alph < 33) {
        panel_ru[ind] = ind_alph - ind;
        panel_ru_reverse[ind_alph] = -(ind_alph - ind);
        ++ind;
        ++ind_alph;
      }
    }
  }
}

