//! @file plugin_Enigma.h
//! @authors Карнаушко В. А.
//! @note Ответственный Полевой Д. В.
//! @brief Головной заголовок для класса шифра Энигмы
//! @todo Copyright Vill

#ifndef ENIGMA_H
#define ENIGMA_H

#include "interface.h"
#include <QObject>
#include <QtWidgets>
#include <QMap>
#include <cstddef>

class Enigma : public QObject, public cipher {
  Q_OBJECT
public:
  Enigma(QObject *parent = nullptr);
  ~Enigma();
  
  //! @brief Применяет алгоритм шифрования Энигмы и возвращает обработанный текст.
  //! @param[in] Text - текст, поступающий на шифрование
  //! @param[in] lang - язык, выбранный в *MenuBar*
  //! @returns Зашифрованный текст
  QString Apply(const QString& Text, const QString& lang) override;

  //! @brief Применяет алгоритм дешифрования Энигмы и возвращает обработанный текст.
  //! @param[in] Text - текст, поступающий на шифрование
  //! @param[in] lang - язык, выбранный в *MenuBar*
  //! @returns Расшифрованный текст
  QString Revent(const QString& Text, const QString& lang) override;

  //! @brief Создает окно с настройками шифра Энигмы.
  void createOptions() override;

  //! @brief Возвращает указатель на окно с настройками шифра.
  QWidget* returnOptions() override;

private:
  QChar* en { nullptr }; //!< английский алфавит
  QChar* ru{ nullptr }; //!< русский алфавит

  std::ptrdiff_t* rotorI { nullptr }; //!< значения сдвигов для английского ротора номер 1
  std::ptrdiff_t* rotorII { nullptr }; //!< значения сдвигов для английского ротора номер 2
  std::ptrdiff_t* rotorIII { nullptr }; //!< значения сдвигов для английского ротора номер 3

  /*! @note
  * Английские (и русские) "обратные" роторы содержат иднекс сдвига для каждой буквы английского (или русского) алфавита,
  * который равен обратному индексу буквы, из которой приходит сигнал
  */

  std::ptrdiff_t* rotorI_reverse { nullptr }; //!< значения обратных сдвигов для английского ротора номер 1
  std::ptrdiff_t* rotorII_reverse { nullptr }; //!< значения обратных сдвигов для английского ротора номер 2
  std::ptrdiff_t* rotorIII_reverse { nullptr }; //!< значения обратных сдвигов для английского ротора номер 3

  /*! @note
  * Примечание: русской энигмы нет и конфигурации я придумал сам для
  * сохранения пользователю возможности для каждого шифра использовать ОБА языка
  */

  std::ptrdiff_t* rotorI_ru { nullptr }; //!< значения сдвигов для русского ротора номер 1
  std::ptrdiff_t* rotorII_ru { nullptr }; //!< значения сдвигов для русского ротора номер 2
  std::ptrdiff_t* rotorIII_ru { nullptr }; //!< значения сдвигов для русского ротора номер 3

  std::ptrdiff_t* rotorI_ru_reverse { nullptr }; //!< значения обратных сдвигов для русского ротора номер 1
  std::ptrdiff_t* rotorII_ru_reverse { nullptr }; //!< значения обратных сдвигов для русского ротора номер 2
  std::ptrdiff_t* rotorIII_ru_reverse { nullptr }; //!< значения обратных сдвигов для русского ротора номер 3

  //! @note Коммутационная панель - доп. усложнение шифровки: как ротор, но не вращается

  std::ptrdiff_t* panel { nullptr }; //!< значения сдвигов по коммутационной панели английской
  std::ptrdiff_t* panel_reverse { nullptr }; //!< значения обратных сдвигов по коммутационной панели английской

  std::ptrdiff_t* panel_ru { nullptr }; //!< значения сдвигов по коммутационной панели русской
  std::ptrdiff_t* panel_ru_reverse { nullptr }; //!< значения сдвигов по коммутационной панели русской

  /*! @note
  * Рефлекторы английский и русский - механизм, обеспечивающий возможность дешифровки,
  * работает так: связывает в пары все буквы алфавита (искл. нечетное число букв - тогда
  * одна буква может быть связана сама с собой, хоть в реальности работа с такими алфавитами невозможна)
  * --прим. В моей реализации русская энигма отходит от реализма, так как её и не существовало
  */

  std::ptrdiff_t* reflector { nullptr }; //!< значения индексов связанных в пары английских букв
  std::ptrdiff_t* reflector_ru { nullptr }; //!< значения индексов связанных в пары русских букв

  std::ptrdiff_t** Chosen_ru { nullptr }; //!< выбранные русские роторы
  std::ptrdiff_t** Chosen_ru_reverse { nullptr }; //!< выбранные русские обратные роторы

  std::ptrdiff_t** Chosen_en { nullptr }; //!< выбранные английские роторы
  std::ptrdiff_t** Chosen_en_reverse { nullptr }; //!< выбранные английские обратные роторы

  std::ptrdiff_t in_use_en {3}; //!< число задействованных английских роторов
  std::ptrdiff_t in_use_ru {3}; //!< число задействованных русских роторов

  QWidget* widget { nullptr }; //!< окно с настройками шифра

  QSpinBox* ru_spin { nullptr }; //!< переключатель числа задействованных русских роторов
  QSpinBox* ru_spin_1 { nullptr }; //!< переключатель номера задействованного русского ротора на позиции 1
  QSpinBox* ru_spin_2 { nullptr }; //!< переключатель номера задействованного русского ротора на позиции 2
  QSpinBox* ru_spin_3 { nullptr }; //!< переключатель номера задействованного русского ротора на позиции 3

  QSpinBox* en_spin { nullptr }; //!< переключатель числа задействованных английских роторов
  QSpinBox* en_spin_1 { nullptr }; //!< переключатель номера задействованного английского ротора на позиции 1
  QSpinBox* en_spin_2 { nullptr }; //!< переключатель номера задействованного английского ротора на позиции 2
  QSpinBox* en_spin_3 { nullptr }; //!< переключатель номера задействованного английского ротора на позиции 3

  QLineEdit* line_rotorI_ru { nullptr }; //!< поле в окне Опций с русским ротором номер 1
  QLineEdit* line_rotorII_ru { nullptr }; //!< поле в окне Опций с русским ротором номер 2
  QLineEdit* line_rotorIII_ru { nullptr }; //!< поле в окне Опций с русским ротором номер 3
  QLineEdit* line_rotorI { nullptr }; //!< поле в окне Опций с английским ротором номер 1
  QLineEdit* line_rotorII { nullptr }; //!< поле в окне Опций с английским ротором номер 2
  QLineEdit* line_rotorIII { nullptr }; //!< поле в окне Опций с английским ротором номер 3

  QLineEdit* line_panel{ nullptr }; //!< поле в окне Опций с английской коммутационной панелью
  QLineEdit* line_panel_ru{ nullptr }; //!< поле в окне Опций с русской коммутационной панелью

  //! @brief Возвращает индекс буквы *ch* в английском алфавите (*en*). Если не найдена, возвращает значение *-1*.
  //! @param[in] ch - буква английского алфавита
  std::ptrdiff_t find_en(QChar ch);

  //! @brief Возвращает индекс буквы *ch* в русском алфавите (*ru*). Если не найдена, возвращает значение *-1*.
  //! @param[in] ch - буква русского алфавита
  std::ptrdiff_t find_ru(QChar ch);

  //! @brief Задает начальные значения сдвигов для роторов и обратных роторов, рефлекторов и коммутационных панелей.
  void setRotors();

  //! @brief Выбирает английский ротор на позицию *num*.
  //! @param[in] num - номер позиции в механизме ротора
  //! @param[in] val - номер выбранного ротора
  void choose_en(const std::ptrdiff_t& num, const int& val);
  
  //! @brief Выбирает русский ротор на позицию *num*.
  //! @param[in] num - номер позиции в механизме ротора
  //! @param[in] val - номер выбранного ротора
  void choose_ru(const std::ptrdiff_t& num, const int& val);
  
  //! @brief Выводит номера выбранных английских роторов в строку.
  //! @param[in] out - строка, куда записываются номера роторов
  void chosen_en_nums(QString& out);
  
  //! @brief Выводит номера выбранных русских роторов в строку.
  //! @param[in] out - строка, куда записываются номера роторов
  void chosen_ru_nums(QString& out);

private slots:
  //! @brief Меняет число используемых русских роторов (*in_use_ru*).
  //! @param[in] arg1 - число, которое заменяет значение в in_use_ru
  void on_spin_ru_valueChanged(int arg1);

  //! @brief Меняет число используемых английских роторов (*in_use_en*).
  //! @param[in] arg1 - число, которое заменяет значение в in_use_en
  void on_spin_en_valueChanged(int arg1);

  //! @brief Меняет номер используемого русского ротора на позиции 1.
  //! @param[in] arg1 - номер используемого ротора
  void on_spin_ru_1_valueChanged(int arg1);

  //! @brief Меняет номер используемого английского ротора на позиции 1.
  //! @param[in] arg1 - номер используемого ротора
  void on_spin_en_1_valueChanged(int arg1);

  //! @brief Меняет номер используемого русского ротора на позиции 2.
  //! @param[in] arg1 - номер используемого ротора
  void on_spin_ru_2_valueChanged(int arg1);

  //! @brief Меняет номер используемого английского ротора на позиции 2.
  //! @param[in] arg1 - номер используемого ротора
  void on_spin_en_2_valueChanged(int arg1);

  //! @brief Меняет номер используемого русского ротора на позиции 3.
  //! @param[in] arg1 - номер используемого ротора
  void on_spin_ru_3_valueChanged(int arg1);

  //! @brief Меняет номер используемого английского ротора на позиции 3.
  //! @param[in] arg1 - номер используемого ротора
  void on_spin_en_3_valueChanged(int arg1);

  //! @brief Меняет значения сдвигов и обратных сдвигов для английской коммутационной панели.
  void on_button_en_clicked();

  //! @brief Меняет значения сдвигов и обратных сдвигов для русской коммутационной панели.
  void on_button_ru_clicked();
};

#endif
