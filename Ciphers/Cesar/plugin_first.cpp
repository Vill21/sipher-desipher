#include "plugin_first.h"

int Cesar::sdvig_en = 1;
int Cesar::sdvig_ru = 1;

int Cesar::getSdvig_en() { return sdvig_en; }
void Cesar::setSdvig_en(const int Sdvig) { sdvig_en = Sdvig; }
int Cesar::getSdvig_ru() { return sdvig_ru; }
void Cesar::setSdvig_ru(const int Sdvig) { sdvig_ru = Sdvig; }

Cesar::Cesar(QObject* parent) :
  QObject(parent),
  cipher("Cesar")
{
  //english lang
  en = new QChar[26];
  QString alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  std::ptrdiff_t ind(0);
  int ind_str(0);
  do {
    en[ind] = alphabet[ind_str];
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  //russian lang
  ru = new QChar[33];
  alphabet = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
  ind = 0;
  ind_str = 0;
  do {
    ru[ind] = alphabet[ind_str];
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());
}

Cesar::~Cesar(){
  delete[] en;
  delete[] ru;
}

QString Cesar::Apply(const QString& Text, const QString& lang) {
  QString txt_out(Text);
  if (lang == "En") {
    for (int i = 0; i < Text.length(); i++)
    {
      std::ptrdiff_t res = find_en(Text[i].toUpper());
      if (res != -1) {
        std::ptrdiff_t index(0);
        index = (res + sdvig_en) % 26;
        if (Text[i].isLower()) {
          txt_out[i] = (*(en + index)).toLower();
        }
        else {
          txt_out[i] = *(en + index);
        }
      }
    }
  }
  else {
    for (int i = 0; i < Text.length(); i++)
    {
      std::ptrdiff_t res = find_ru(Text[i].toUpper());
      if (res != -1) {
        std::ptrdiff_t index(0);
        index = (res + sdvig_ru) % 33;
        if (Text[i].isLower()){
          txt_out[i] = (*(ru + index)).toLower();
        }
        else {
          txt_out[i] = *(ru + index);
        }
      }
    }
  }

  return txt_out;
}

QString Cesar::Revent(const QString& Text, const QString& lang) {
  QString txt_out(Text);
  if (lang == "En") {
    for (int i = 0; i < Text.length(); i++)
    {
      std::ptrdiff_t res = find_en(Text[i].toUpper());
      if (res == -1) {
        txt_out[i] = Text[i];
      }
      else {
        std::ptrdiff_t index(0);
        if (res - sdvig_en < 0) {
          index = res - sdvig_en + 26;
        }
        else{
          index = res - sdvig_en;
        }
        if (Text[i].isLower()) {
          txt_out[i] = (*(en + index)).toLower();
        }
        else {
          txt_out[i] = *(en + index);
        }
      }
    }
  }
  else {
    for (int i = 0; i < Text.length(); i++)
    {
      std::ptrdiff_t res = find_ru(Text[i].toUpper());
      if (res == -1) {
        txt_out[i] = Text[i];
      }
      else {
        std::ptrdiff_t index(0);
        if (res - sdvig_ru < 0) {
          index = res - sdvig_ru + 33;
        }
        else{
          index = res - sdvig_ru;
        }
        if (Text[i].isLower()){
          txt_out[i] = (*(ru + index)).toLower();
        }
        else {
          txt_out[i] = *(ru + index);
        }
      }
    }
  }

  return txt_out;
}

std::ptrdiff_t Cesar::find_en(QChar ch){
  std::ptrdiff_t ind(0);
  std::ptrdiff_t find(-1);
  do {
    if (en[ind] == ch){
      find = ind;
      break;
    }
    ++ind;
  } while (ind < 26);
  return find;
}
std::ptrdiff_t Cesar::find_ru(QChar ch){
  std::ptrdiff_t ind(0);
  std::ptrdiff_t find(-1);
  do {
    if (ru[ind] == ch){
      find = ind;
      break;
    }
    ++ind;
  } while (ind < 33);
  return find;
}
