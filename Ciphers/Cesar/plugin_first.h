//! @file plugin_first.h
//! @authors Карнаушко В. А.
//! @note Ответственный Полевой Д. В.
//! @brief Головной заголовок для класса шифра Цезаря
//! @todo Copyright Vill

#ifndef PLUGIN_CESAR_H
#define PLUGIN_CESAR_H

#include "interface.h"
#include <cstddef>
#include <QObject>

class Cesar : public QObject, public cipher {
  Q_OBJECT
public:
  Cesar(QObject *parent = nullptr);
  ~Cesar();

  //! @brief Применяет алгоритм шифрования Цезаря и возвращает обработанный текст.
  //! @param[in] Text - текст, поступающий на шифрование
  //! @param[in] lang - язык, выбранный в *MenuBar*
  //! @returns Зашифрованный текст
  QString Apply(const QString& Text, const QString& lang) override;

  //! @brief Применяет алгоритм дешифрования Цезаря и возвращает обработанный текст.
  //! @param[in] Text - текст, поступающий на шифрование
  //! @param[in] lang - язык, выбранный в *MenuBar*
  //! @returns Расшифрованный текст
  QString Revent(const QString& Text, const QString& lang) override;

  //! @brief Возвращает значение сдвига для английского алфавита.
  static int getSdvig_en();

  //! @brief Задает значение сдвига для английского алфавита.
  //! @param[in] Sdvig - число, на которое заменяется значение сдвига
  static void setSdvig_en(const int Sdvig);

  //! @brief Возвращает значение сдвига для русского алфавита.
  static int getSdvig_ru();

  //! @brief Задает значение сдвига для русского алфавита.
  //! @param[in] Sdvig - число, на которое заменяется значение сдвига
  static void setSdvig_ru(const int Sdvig);

private:
  static int sdvig_ru; //!< значение сдвига для русского алфавита
  static int sdvig_en; //!< значение сдвига для английского алфавита

  QChar* ru{ nullptr }; //!< русский алфавит
  QChar* en{ nullptr }; //!< английский алфавит

  //! @brief Возвращает индекс буквы *ch* в английском алфавите (*en*). Если не найдена, возвращает значение *-1*.
  //! @param[in] ch - буква английского алфавита
  std::ptrdiff_t find_en(QChar ch);

  //! @brief Возвращает индекс буквы *ch* в русском алфавите (*ru*). Если не найдена, возвращает значение *-1*.
  //! @param[in] ch - буква русского алфавита
  std::ptrdiff_t find_ru(QChar ch);
};

#endif // PLUGIN_FIRST_H
