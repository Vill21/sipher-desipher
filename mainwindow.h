//! @file mainwindow.h
//! @authors Карнаушко В. А.
//! @note Ответственный Полевой Д. В.
//! @brief Головной заголовок для функционала приложения
//! @todo Copyright Vill

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QPluginLoader>
#include <QFileDialog>
#include <QTextStream>
#include <QTextCodec>
#include <QDir>
#include <QJsonObject>
#include <QToolBar>
#include <QComboBox>
#include <QDebug>
#include <QMap>
#include "interface.h"
#include <Ciphers/Cesar/plugin_first.h>
#include <Ciphers/Enigma/plugin_Enigma.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget* parent = nullptr);
  ~MainWindow();

  //! @brief Функция загрузки статических библиотек с классами шифров
  void loadCiphers();

  //! @brief Функция загрузки динамических библиотек (*плагинов*) с классами шифров
  void loadPlugins();

private slots:

  //! @brief Осуществляет переход на окно Опций (*Options*).
  void on_actionOptions_triggered();

  //! @brief Осуществляет переход на Главное окно (*MainWindow*).
  void on_actionReturn_triggered();

  //! @brief Применяет метод шифрования выбранного шифра и выводит результат в окно вывода текста (*правое*).
  void on_Apply_clicked();

  //! @brief Применяет метод дешифрования выбранного шифра и выводит результат в окно вывода текста (*правое*).
  void on_Revent_clicked();

  //! @brief Переключает stacked widget в окне Опций на страницу по индексу нажатого шифра, *если такая страница есть*.
  void on_listWidget_itemClicked();

  /*! @brief
  * Открывает окно диалога с файловой системой, где пользователь выбирает файл .txt, выводит текст файла в окно ввода, если файл удалось прочесть.
  * Также, если файл удалось прочесть, выводит путь до открытого файла в окно выше окна ввода текста, сохраняя в переменную
  * *failpath* этот путь.
  */
  void on_actionImport_triggered();

  //! @brief Сохраняет текст из окна вывода в файл по пути, записанному в переменной filepath. Если переменная не содержит пути, то вызывает действие SaveAs.
  void on_actionSave_triggered();

  //! @brief Вызывает диалог с файловой системой, где пользователь указывает, куда сохранить результат работы приложения в формате .txt.
  void on_actionSave_As_triggered();

  //! @brief Меняет язык, с которым работает шифратор на английский
  void on_actionEn_2_triggered();

  //! @brief Меняет язык, с которым работает шифратор на русский.
  void on_actionRu_2_triggered();

  //! @brief Очищает окна ввода и вывода текста, окно с путём до импортированного файла, меняет значение переменной filepath на "".
  void on_actionClose_File_triggered();

  //! @brief Меняет значение переменной *sdvig_en* на значение *arg1* при изменения значения в soinBox_Sdvig. 
  //! @param[in] arg1 - значение из spinBox_Sdvig.
  void on_spinBox_Sdvig_valueChanged(int arg1);

  //! @brief Меняет значение переменной *sdvig_ru* на значение *arg1* при изменении значения в spinBox.
  //! @param[in] arg1 - значение из spinBox. 
  void on_spinBox_valueChanged(int arg1);

private:
  Ui::MainWindow* ui{ nullptr };

  QComboBox* ciphers{ nullptr };//!< список с именами загруженных шифров

  QAction* ciphers_del{ nullptr };//!< действие добавления списка с шифрами в ToolBar

  QList<cipher*> _pointers;//!< список, содержащий в себе классы шифров

  QMap<int, QString> options_menu;//!< Хэш-таблица, связывающая окно настройки шифра с его индексом в списке по имени шифра

  QString filepath{ "" };//!< Путь к загруженному файлу

  /*! @brief
  * Добавляет класс шифра в переменную *_pointers*, имя шифра в список шифров на ToolBar и в меню опций.
  * Встраивает widget с настройками шифра в окно Опций по индексу новой страницы stacked_widget в этом окне.
  */
  //! @param[in] point - указатель на класс шифра
  //! @param[in] num - номер по счету поступившего шифра
  void load(cipher* point, const int& num);
};
#endif // MAINWINDOW_H
