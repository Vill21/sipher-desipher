#include "mainwindow.h"

#include <QApplication>

//! @brief Запускает приложение
//! @param[in] argc - количество параметров
//! @param[in] argv[] - строки, содержащие данные
int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  MainWindow w;
  w.show();
  return a.exec();
}
