#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  ciphers = new QComboBox(this);
  ui->toolBar->addSeparator();
  ciphers_del = ui->toolBar->addWidget(ciphers);
  loadCiphers();
  loadPlugins();
  ui->menuLanguage->setTitle("En");
}

MainWindow::~MainWindow()
{
  delete ui;
  delete[] ciphers;
  delete ciphers_del;
}

void MainWindow::loadCiphers(){
  Cesar* ces = new Cesar(this);
  Enigma* enigma = new Enigma(this);

  load(ces, 0);
  load(enigma, 1);
}
void MainWindow::loadPlugins(){
  QDir dir(qApp->applicationDirPath());
  dir.cd("Plugins");
  QStringList filter;
  filter << "*.dll";
  QPluginLoader loader;
  int counter(ciphers->count());

  foreach(QFileInfo info, dir.entryInfoList(filter)){
    loader.setFileName(info.absoluteFilePath());
    cipher* pointer = qobject_cast<cipher*>(loader.instance());
    if (pointer != nullptr){
      load(pointer, counter);
      counter++;
    }
    else {
      qDebug() << loader.errorString();
    }
  }
}

void MainWindow::on_actionOptions_triggered()
{
  ui->stackedWidget->setCurrentIndex(1);
  ui->toolBar->removeAction(ciphers_del);
}

void MainWindow::on_actionReturn_triggered()
{
  ui->stackedWidget->setCurrentIndex(0);
  ui->toolBar->addAction(ciphers_del);
}


void MainWindow::on_Apply_clicked()
{
  QString txt = _pointers.at(ciphers->currentIndex())->Apply(ui->plainTextEdit->toPlainText(), ui->menuLanguage->title());
  ui->plainTextEdit_2->setPlainText(txt);
}

void MainWindow::on_Revent_clicked()
{
  QString txt = _pointers.at(ciphers->currentIndex())->Revent(ui->plainTextEdit->toPlainText(), ui->menuLanguage->title());
  ui->plainTextEdit_2->setPlainText(txt);
}

void MainWindow::on_listWidget_itemClicked()
{
  int index_chosen = ui->listWidget->currentRow();
  int index_shown = ui->options_menu->currentIndex();
  if (index_chosen != index_shown){
    ui->options_menu->setCurrentIndex(options_menu.key(_pointers.at(index_chosen)->getName()));
  }
}

void MainWindow::on_actionImport_triggered()
{
  QString filter = "All files (*.*) ;; Text files (*.txt)";
  filepath = QFileDialog::getOpenFileName(this, "Open a text file", QDir::homePath(), filter);
  QFile file(filepath);

  if (!file.open(QFile::ReadOnly | QFile::Text)){
    QMessageBox::warning(this, "Error", "File cannot be opened");
  }
  else{
    QTextCodec* codec = QTextCodec::codecForName("UTF-8");
    QTextStream in(&file);
    in.setCodec(codec);
    QString text = in.readAll();
    ui->plainTextEdit->setPlainText(text);
  }
  file.close();

  ui->textBrowser->setPlainText(file.fileName());
}

void MainWindow::on_actionSave_triggered()
{
  if (filepath != ""){
    QFile file(filepath);
    if(file.open(QIODevice::WriteOnly)){
      QTextCodec* codec = QTextCodec::codecForName("UTF-8");
      QTextStream out(&file);
      out.setCodec(codec);
      QString text = ui->plainTextEdit_2->toPlainText();
      out << text;
      file.close();
    }
  }
  else{
    on_actionSave_As_triggered();
  }
}

void MainWindow::on_actionSave_As_triggered()
{
  QString filter = "All files (*.*) ;; Text files (*.txt)";
  QString filepath = QFileDialog::getSaveFileName(this, tr("Save the text file"), QDir::homePath(), filter);

  //Пользователь может ввести пустую строку или не выбрать файл
  if (filepath.isEmpty()){
    return;
  }
  else {
    QFile file(filepath);
    if (!file.open(QIODevice::WriteOnly)) {
      QMessageBox::information(this, tr("Unable to open file"), file.errorString());
      return;
    }
    else{
      QTextCodec* codec = QTextCodec::codecForName("UTF-8");
      QTextStream out(&file);
      out.setCodec(codec);
      QString text = ui->plainTextEdit_2->toPlainText();
      out << text;
      file.close();
    }
  }
}

void MainWindow::on_actionEn_2_triggered()
{
  ui->menuLanguage->setTitle("En");
}

void MainWindow::on_actionRu_2_triggered()
{
  ui->menuLanguage->setTitle("Ru");
}

void MainWindow::on_actionClose_File_triggered()
{
  filepath = "";
  ui->plainTextEdit->setPlainText("");
  ui->plainTextEdit_2->setPlainText("");
  ui->textBrowser->setPlainText(filepath);
}

void MainWindow::on_spinBox_Sdvig_valueChanged(int arg1)
{
  Cesar::setSdvig_ru(arg1);
  QString val = QString::number(arg1);
  ui->lineEdit->setText("Russian shift value: " + val);
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{
  Cesar::setSdvig_en(arg1);
  QString val = QString::number(arg1);
  ui->lineEdit_2->setText("English shift value: " + val);
}

void MainWindow::load(cipher* point, const int& num){
  ciphers->addItem(point->getName());
  _pointers.append(point);
  ui->listWidget->addItem(point->getName());
  options_menu[num] = point->getName();
  if (point->returnOptions() != nullptr) {
    ui->options_menu->addWidget(point->returnOptions());
  }
}
