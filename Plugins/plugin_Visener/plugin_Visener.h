//! @file plugin_Visener.h
//! @authors Карнаушко В. А.
//! @note Ответственный Полевой Д. В.
//! @brief Головной заголовок для класса шифра Виженера
//! @todo Copyright Vill

#ifndef PLUGIN_VISENER_H
#define PLUGIN_VISENER_H

#include "interface.h"
#include "plugin_Visener_global.h"
#include <QObject>
#include <cstddef>
#include <QtWidgets>

class PLUGIN_VISENER_EXPORT Visener : public QObject, public cipher {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID cipher_iid FILE "plugin_Visener.json")
  Q_INTERFACES(cipher)
public:
  Visener(QObject *parent = nullptr);
  ~Visener();

  //! @brief Применяет алгоритм шифрования Виженера и возвращает обработанный текст.
  //! @param[in] Text - текст, поступающий на шифрование
  //! @param[in] lang - язык, выбранный в *MenuBar*
  //! @returns Зашифрованный текст
  QString Apply(const QString& Text, const QString& lang) override;

  //! @brief Применяет алгоритм дешифрования Виженера и возвращает обработанный текст.
  //! @param[in] Text - текст, поступающий на шифрование
  //! @param[in] lang - язык, выбранный в *MenuBar*
  //! @returns Расшифрованный текст
  QString Revent(const QString& Text, const QString& lang) override;

  //! @brief Создает окно с настройками шифра Виженера.
  void createOptions() override;

  //! @brief Возвращает указатель на окно с настройками шифра.
  QWidget* returnOptions() override;

private:
  QString code_en { "RABBIT" }; //!< Кодовое слово для английского алфавита
  QString code_ru { "ЧАЙ" }; //!< Кодовое слово для русского алфавита
  std::ptrdiff_t* en_inds { nullptr }; //!< Значения сдвигов для английского кодового слова
  std::ptrdiff_t* ru_inds { nullptr }; //!< Значение сдвигов для русского кодового слова

  QWidget* widget { nullptr }; //!< Окно с настройками шифра
  QLineEdit* line_ru { nullptr }; //!< Кодовое слово русское в окне с настройками
  QLineEdit* line_en { nullptr }; //!< Кодовое слово английское в окне с настройками

  QChar* ru{ nullptr }; //!< Русский алфавит
  QChar* en{ nullptr }; //!< Английский алфавит

  //! @brief Возвращает индекс буквы *ch* в английском алфавите (*en*). Если не найдена, возвращает значение *-1*.
  //! @param[in] ch - буква английского алфавита
  std::ptrdiff_t find_en(QChar ch); 

  //! @brief Возвращает индекс буквы *ch* в русском алфавите (*ru*). Если не найдена, возвращает значение *-1*.
  //! @param[in] ch - буква русского алфавита
  std::ptrdiff_t find_ru(QChar ch);

  //! @brief Меняет значения сдвигов для русского кодового слова.
  void changeRu();

  //! @brief Меняет значения сдвигов для английского кодового слова.
  void changeEn();

private slots:
  /*! @brief 
  * Проверяет, что введенное в *line_ru* слово состоит только из букв русского алфавита,
  * меняет кодовое слово и вызывает *changeRu()*.
  */ 
  void on_button_ru_clicked();

  /*! @brief
  * Проверяет, что введенное в *line_en* слово состоит только из букв английского алфавита,
  * меняет кодовое слово и вызывает *changeEn()*.
  */
  void on_button_en_clicked();
};

#endif // PLUGIN_FIRST_H
