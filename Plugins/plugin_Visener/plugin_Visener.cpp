#include "plugin_Visener.h"

Visener::Visener(QObject* parent) :
  QObject(parent),
  cipher("Visener")
{
  createOptions();

  //english lang
  en = new QChar[26];
  QString alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  std::ptrdiff_t ind(0);
  int ind_str(0);
  do {
    en[ind] = alphabet[ind_str];
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  //russian lang
  ru = new QChar[33];
  alphabet = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
  ind = 0;
  ind_str = 0;
  do {
    ru[ind] = alphabet[ind_str];
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  changeEn();
  changeRu();
}

void Visener::changeRu(){
  //each ru_code index
  ru_inds = new std::ptrdiff_t[code_ru.length()];
  std::ptrdiff_t ind = 0;
  int ind_str = 0;

  do {
    ru_inds[ind] = find_ru(code_ru[ind_str]);
    ++ind;
    ++ind_str;
  } while(ind < code_ru.length());
}
void Visener::changeEn(){
  //each en_code index
  en_inds = new std::ptrdiff_t[code_en.length()];
  std::ptrdiff_t ind = 0;
  int ind_str = 0;

  do {
    en_inds[ind] = find_en(code_en[ind_str]);
    ++ind;
    ++ind_str;
  } while(ind < code_en.length());
}

Visener::~Visener(){
  delete[] ru;
  delete[] en;
  delete[] ru_inds;
  delete[] en_inds;
}

void Visener::createOptions(){
  widget = new QWidget;
  QPushButton* button_ru = new QPushButton("Accept");
  line_ru = new QLineEdit();
  line_ru->setText(code_ru);

  QPushButton* button_en = new QPushButton("Accept");
  line_en = new QLineEdit();
  line_en->setText(code_en);

  connect(button_en, SIGNAL(clicked()), this, SLOT(on_button_en_clicked()));
  connect(button_ru, SIGNAL(clicked()), this, SLOT(on_button_ru_clicked()));

  QFormLayout* layout = new QFormLayout;
  layout->addRow(button_ru, line_ru);
  layout->addRow(button_en, line_en);

  widget->setLayout(layout);
}

QWidget* Visener::returnOptions() {
  return widget;
}

std::ptrdiff_t Visener::find_en(QChar ch){
  std::ptrdiff_t ind(0);
  std::ptrdiff_t find(-1);
  do {
    if (en[ind] == ch){
      find = ind;
      break;
    }
    ++ind;
  } while (ind < 26);
  return find;
}
std::ptrdiff_t Visener::find_ru(QChar ch){
  std::ptrdiff_t ind(0);
  std::ptrdiff_t find(-1);
  do {
    if (ru[ind] == ch){
      find = ind;
      break;
    }
    ++ind;
  } while (ind < 33);
  return find;
}


QString Visener::Apply(const QString& Text, const QString& lang) {
  QString txt_out(Text);
  std::ptrdiff_t index_code(-1);
  if (lang == "En") {
    for (int i = 0; i < Text.length(); i++)
    {
      std::ptrdiff_t res = find_en(Text[i].toUpper());
      if (res == -1) {
        txt_out[i] = Text[i];
      }
      else {
        index_code = (index_code + 1) % code_en.length();
        std::ptrdiff_t index(0);
        if (res + en_inds[index_code] > 25) {
          index = res + en_inds[index_code] - 26;
        }
        else{
          index = res + en_inds[index_code];
        }
        if (Text[i].isLower()) {
          txt_out[i] = (*(en + index)).toLower();
        }
        else {
          txt_out[i] = *(en + index);
        }
      }
    }
  }
  else {
    for (int i = 0; i < Text.length(); i++)
    {
      std::ptrdiff_t res = find_ru(Text[i].toUpper());
      if (res == -1) {
        txt_out[i] = Text[i];
      }
      else {
        index_code = (index_code + 1) % code_ru.length();
        std::ptrdiff_t index(0);
        if (res + ru_inds[index_code] > 32) {
          index = res + ru_inds[index_code] - 33;
        }
        else{
          index = res + ru_inds[index_code];
        }
        if (Text[i].isLower()){
          txt_out[i] = (*(ru + index)).toLower();
        }
        else {
          txt_out[i] = *(ru + index);
        }
      }
    }
  }

  return txt_out;
}

QString Visener::Revent(const QString& Text, const QString& lang) {
  QString txt_out(Text);
  std::ptrdiff_t index_code(-1);
  if (lang == "En") {
    for (int i = 0; i < Text.length(); i++)
    {
      std::ptrdiff_t res = find_en(Text[i].toUpper());
      if (res != -1) {
        index_code = (index_code + 1) % code_en.length();
        std::ptrdiff_t index(0);
        if (res - en_inds[index_code] < 0) {
          index = res - en_inds[index_code] + 26;
        }
        else{
          index = res - en_inds[index_code];
        }
        if (Text[i].isLower()) {
          txt_out[i] = (*(en + index)).toLower();
        }
        else {
          txt_out[i] = *(en + index);
        }
      }
    }
  }
  else {
    for (int i = 0; i < Text.length(); i++)
    {
      std::ptrdiff_t res = find_ru(Text[i].toUpper());
      if (res != -1) {
        index_code = (index_code + 1) % code_ru.length();
        std::ptrdiff_t index(0);
        if (res - ru_inds[index_code] < 0) {
          index = res - ru_inds[index_code] + 33;
        }
        else{
          index = res - ru_inds[index_code];
        }
        if (Text[i].isLower()){
          txt_out[i] = (*(ru + index)).toLower();
        }
        else {
          txt_out[i] = *(ru + index);
        }
      }
    }
  }

  return txt_out;
}

void Visener::on_button_ru_clicked(){
  std::ptrdiff_t res(-1);
  QString txt = line_ru->text().toUpper();
  for (int i = 0; i < txt.length(); ++i) {
    res = find_ru(txt[i]);
    if (res == -1) {
      break;
    }
  }
  if (res == -1) {
    line_ru->setText("Error: non russian words used - " + txt);
  }
  else {
    line_ru->setText(txt);
    code_ru = txt;
    changeRu();
  }
}

void Visener::on_button_en_clicked(){
  std::ptrdiff_t res(-1);
  QString txt = line_en->text().toUpper();
  for (int i = 0; i < txt.length(); ++i) {
    res = find_en(txt[i]);
    if (res == -1) {
      break;
    }
  }
  if (res == -1) {
    line_en->setText("Error: non english words used - " + txt);
  }
  else {
    line_en->setText(txt);
    code_en = txt;
    changeEn();
  }
}


