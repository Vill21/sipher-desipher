#ifndef PLUGIN_MORSE_GLOBAL_H
#define PLUGIN_MORSE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(PLUGIN_MORSE_LIBRARY)
#  define PLUGIN_MORSE_EXPORT Q_DECL_EXPORT
#else
#  define PLUGIN_MORSE_EXPORT Q_DECL_IMPORT
#endif

#endif // PLUGIN_MORSE_GLOBAL_H
