//! @file plugin_morse.h
//! @authors Карнаушко В. А.
//! @note Ответственный Полевой Д. В.
//! @brief Головной заголовок для класса шифра Морзе
//! @todo Copyright Vill

#ifndef PLUGIN_MORSE_H
#define PLUGIN_MORSE_H

#include "interface.h"
#include "plugin_Morse_global.h"
#include <QObject>
#include <QtWidgets>
#include <QMap>

class PLUGIN_MORSE_EXPORT Morse : public QObject, public cipher {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID cipher_iid FILE "plugin_Morse.json")
  Q_INTERFACES(cipher)
public:
  Morse(QObject *parent = nullptr);
  ~Morse();

  //! @brief Применяет алгоритм шифрования Морзе и возвращает обработанный текст.
  //! @param[in] Text - текст, поступающий на шифрование
  //! @param[in] lang - язык, выбранный в *MenuBar*
  //! @returns Зашифрованный текст
  QString Apply(const QString& Text, const QString& lang) override;

  //! @brief Применяет алгоритм дешифрования Морзе и возвращает обработанный текст.
  //! @param[in] Text - текст, поступающий на шифрование
  //! @param[in] lang - язык, выбранный в *MenuBar*
  //! @returns Расшифрованный текст
  QString Revent(const QString& Text, const QString& lang) override;

  //! @brief Создает окно с настройками шифра Морзе.
  void createOptions() override;

  //! @brief Возвращает указатель на окно с настройками шифра.
  QWidget* returnOptions() override;

private:
  QMap<QChar, QString> map_en; //!< Хеш-таблица, соединяющая английский символ и его код

  QChar* ru_char { nullptr }; //!< Русский алфавит
  QString* ru_digit { nullptr }; //!< Коды для русских символов

  QWidget* widget { nullptr }; //!< Окно с настройками шифра
  QLineEdit* line { nullptr }; //!< Строка в окне настроек с текстом о выбранном стиле шифрования : *числа* или *символы*

  bool state { false }; //!< Флаг способа представления: *false* - цифрами, *true* - символами

  //! @brief Возвращает код русского символа. Если такого символа нет, возвращает *-1*.
  //! @param[in] ch - русский символ
  std::ptrdiff_t value_ind(QChar ch);

  //! @brief Возвращает русский символ по коду. Если такого кода нет, возвращает *-1*.
  //! @param[in] str - код русского символа
  std::ptrdiff_t key_ind(QString str);

  //! @brief Создает Хеш-таблицу и заполняет русский алфавит и коды к нему.
  void CreateMaps();

private slots:
  //! @brief Меняет стиль представления закодированного сообщения.
  void on_button_clicked();
};

#endif // PLUGIN_MORSE_H
