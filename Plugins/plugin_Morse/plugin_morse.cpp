#include "plugin_morse.h"

Morse::Morse(QObject *parent) : QObject (parent), cipher("Morse") {

  CreateMaps();
  createOptions();
}

Morse::~Morse(){
  delete[] ru_char;
  delete[] ru_digit;
}

void Morse::CreateMaps(){
  //1 equals -
  //0 equals .

  //English var
  map_en['A'] = "01";
  map_en['B'] = "1000";
  map_en['C'] = "1010";
  map_en['D'] = "100";
  map_en['E'] = "0";
  map_en['F'] = "0010";
  map_en['G'] = "110";
  map_en['H'] = "0000";
  map_en['I'] = "00";
  map_en['J'] = "0111";
  map_en['K'] = "101";
  map_en['L'] = "0100";
  map_en['M'] = "11";
  map_en['N'] = "10";
  map_en['O'] = "111";
  map_en['P'] = "0110";
  map_en['Q'] = "1101";
  map_en['R'] = "010";
  map_en['S'] = "000";
  map_en['T'] = "1";
  map_en['U'] = "001";
  map_en['V'] = "0001";
  map_en['W'] = "011";
  map_en['X'] = "1001";
  map_en['Y'] = "1011";
  map_en['Z'] = "1100";

  map_en['1'] = "01111";
  map_en['2'] = "00111";
  map_en['3'] = "00011";
  map_en['4'] = "00001";
  map_en['5'] = "00000";
  map_en['6'] = "10000";
  map_en['7'] = "11000";
  map_en['8'] = "11100";
  map_en['9'] = "11110";
  map_en['0'] = "11111";

  //Russian var
  ru_char = new QChar[42];
  QString alphabet = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ1234567890";
  std::ptrdiff_t ind = 0;
  int ind_str = 0;
  do {
    ru_char[ind] = alphabet[ind_str];
    ++ind;
    ++ind_str;
  } while (ind_str < alphabet.length());

  ru_digit = new QString[42];
  alphabet = "01 1000 011 110 100 0 0001 1100 00 0111 101 "
             "0100 11 10 111 0110 010 000 1 001 0010 0000 "
             "1010 110 1111 1101 11011 1011 1001 00100 0011 "
             "0101 01111 00111 00011 00001 00000 10000 11000 "
             "11100 11110 11111";
  QStringList list = alphabet.split(' ');
  ind = 0;
  ind_str = 0;
  do {
    ru_digit[ind] = list[ind_str];
    ++ind;
    ++ind_str;
  } while (ind_str < list.size());
}

QString Morse::Apply(const QString& Text, const QString& lang) {
  QString txt_out("");
  QString txt_in(Text.toUpper());

  if (lang == "En") {
    for(int i = 0; i < txt_in.length(); i++){
      if (state) {
        QString temp = map_en.value(txt_in[i]);
        QString temp_res = "";
        for (int j = 0; j < temp.length(); ++j) {
          if (temp[j] == '1') {
            temp_res += '-';
          }
          if (temp[j] == '0') {
            temp_res += '.';
          }
        }
        txt_out += temp_res + " ";
      }
      else {
        txt_out += map_en.value(txt_in[i]) + " ";
      }
    }
  }
  else {
    for(int i = 0; i < txt_in.length(); i++){
      std::ptrdiff_t res = value_ind(txt_in[i]);
      if (res != -1 ){
        if (state) {
          QString temp = *(ru_digit + res);
          QString temp_res = "";
          for (int j = 0; j < temp.length(); ++j) {
            if (temp[j] == '1') {
              temp_res += '-';
            }
            if (temp[j] == '0') {
              temp_res += '.';
            }
          }
          txt_out += temp_res + " ";
        }
        else {
          txt_out += *(ru_digit + res) + " ";
        }
      }
    }
  }

  return txt_out;
}

QString Morse::Revent(const QString& Text, const QString& lang){
  QString txt_out("");
  QStringList txt_in = Text.split(' ');

  if (lang == "En"){
    for(int i = 0; i < txt_in.length(); i++){
      if (state) {
        QString temp = txt_in[i];
        QString temp_res = "";
        for (int j = 0; j < temp.length(); ++j) {
          if (temp[j] == '-') {
            temp_res += '1';
          }
          if (temp[j] == '.') {
            temp_res += '0';
          }
        }
        txt_out += map_en.key(temp_res);
      }
      else {
        txt_out += map_en.key(txt_in[i]);
      }
    }
  }
  else {
    for(int i = 0; i < txt_in.length(); i++){
      std::ptrdiff_t res(-1);
      if (state) {
        QString temp = txt_in[i];
        QString temp_res = "";
        for (int j = 0; j < temp.length(); ++j) {
          if (temp[j] == '-') {
            temp_res += '1';
          }
          if (temp[j] == '.') {
            temp_res += '0';
          }
        }
        res = key_ind(temp_res);
      }
      else {
        res = key_ind(txt_in[i]);
      }
      if (res != -1) {
        txt_out += *(ru_char + res);
      }
    }
  }

  return txt_out;
}

std::ptrdiff_t Morse::value_ind(QChar ch) {
  std::ptrdiff_t ind(0);
  std::ptrdiff_t res(-1);
  do {
    if (ru_char[ind] == ch) {
      res = ind;
      break;
    }
    ++ind;
  } while (ind < 42);
  return res;
}

std::ptrdiff_t Morse::key_ind(QString str) {
  std::ptrdiff_t ind(0);
  std::ptrdiff_t res(-1);
  do {
    if (ru_digit[ind] == str) {
      res = ind;
      break;
    }
    ++ind;
  } while (ind < 42);
  return res;
}

void Morse::createOptions(){
  widget = new QWidget;
  QPushButton* button_digits = new QPushButton("Change");
  line = new QLineEdit();
  line->setText("Style: digits");
  line->setReadOnly(true);
  connect(button_digits, SIGNAL(clicked()), this, SLOT(on_button_clicked()));

  QFormLayout* layout = new QFormLayout;
  layout->addRow(button_digits, line);

  widget->setLayout(layout);
}

QWidget* Morse::returnOptions() {
  return widget;
}

void Morse::on_button_clicked(){
  state = !state;
  if (state) {
    line->setText("Style: symbols");
  }
  else {
    line->setText("Style: digits");
  }
}
